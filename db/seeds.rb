# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

postagem = Post.find_by(titulo: 'Meu primeiro post')
postagem.comments.create!(name: "Paulo Xavier", comment: "Excelente")
postagem.comments.create!(name: "Joana Santana", comment: "Muito bom")
postagem.comments.create!(name: "Augusto Barros", comment: "Abordagem de assunto excelente")

postagem = Post.find_by(titulo: 'Meu segundo post')
postagem.comments.create!(name: "Marta Freitas", comment: "Cada dia melhor os assuntos abordados")