Rails.application.routes.draw do
  
  devise_for :users
  root 'posts#index'
  #nested resource(recurso aninhado)
  resources :posts do
  	resources :comments # os comentarios precisam dos posts
  end
end
