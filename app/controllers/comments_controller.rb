class CommentsController < ApplicationController

	def index
		@post= Post.find(params[:post_id])
		@comments = @post.comments
	end 

	#Novo comentario do post que se quer comentar
	def new
		@post = Post.find(params[:post_id])
		@comment = @post.comments.new 
	end

	def create
		@post=Post.find(params[:post_id])
		@comment=@post.comments.new(comment_params)
		if @comment.save #se salvo com sucesso
			redirect_to post_comments_path(@post), notice: 'Comentário gravado com sucesso!'
		else
			render :new
		end
	end

	private
		def comment_params
			params.require(:comment).permit(:name, :comment)
		end
end
