class Post < ApplicationRecord
	acts_as_paranoid
	validates :titulo, :conteudo, :data, presence: true  #os campos titulo, conteudo e data sao obrigatorios estarem presentes
	has_many :comments , dependent: :destroy #um post pode ter muitos comentarios, mas se esse post for deletado os comentarios serao deletados tbm
	validates_length_of :titulo, maximum: 200 #o campo titulo pode ter no maximo 200 caracteres
	validates_length_of :conteudo, maximum: 10000 #o campo conteudo pode ter no maximo 600 caracteres

 	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }
  	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/ #enviar apenas imagem
end
