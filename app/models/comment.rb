class Comment < ApplicationRecord
	validates :name, :comment, presence:true #nome e comentario devem estar presentes
	validates_length_of :comment , maximum:200 #o comentario deve ter no maximo 200 caracteres
  	belongs_to :post
end
